﻿namespace HS.Mediator.Common
{
    using System.Threading;
    using System.Threading.Tasks;

    using MediatR;

    public abstract class HandlerBase<TRequest, TResponse> : IRequestHandler<TRequest, TResponse>
        where TRequest : RequestBase<TResponse>
        where TResponse : ResponseBase
    {
        public abstract Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken);
    }
}
