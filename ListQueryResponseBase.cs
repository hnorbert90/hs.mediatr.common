﻿namespace HS.Mediator.Common
{
    using System.Collections.Generic;

    public abstract class ListQueryResponseBase<TRecord> : ResponseBase
    {
        public virtual List<TRecord> Records { get; set; }
    }
}
