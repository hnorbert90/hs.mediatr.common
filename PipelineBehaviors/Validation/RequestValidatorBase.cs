﻿namespace HS.Mediator.Common
{
    using System.Threading;
    using System.Threading.Tasks;

    public abstract class RequestValidatorBase<TRequest, TResponse> : IRequestValidator<TRequest, TResponse> where TRequest : RequestBase<TResponse> where TResponse : ResponseBase
    {
        public abstract Task<TResponse> ValidateAsync(TRequest request, CancellationToken cancellationToken = default);
    }
}
