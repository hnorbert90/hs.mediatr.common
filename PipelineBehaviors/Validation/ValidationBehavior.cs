﻿namespace HS.Mediator.Common.PipelineBehaviors
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using MediatR;

    public class ValidationBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
        where TRequest : RequestBase<TResponse>
        where TResponse : ResponseBase
    {
        private readonly IEnumerable<IRequestValidator<TRequest, TResponse>> _validators;

        public ValidationBehavior(IEnumerable<IRequestValidator<TRequest, TResponse>> validators)
        {
            _validators = validators;
        }

        public virtual async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            if (_validators != null && _validators.Any())
            {
                foreach (var validator in _validators)
                {
                    var response = await validator.ValidateAsync(request, cancellationToken);
                    if (response.HasError)
                    {
                        return response;
                    }
                }
            }

            return await next();
        }
    }
}