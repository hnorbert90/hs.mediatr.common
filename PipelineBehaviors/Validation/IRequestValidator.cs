﻿namespace HS.Mediator.Common
{
    using System.Threading;
    using System.Threading.Tasks;

    public interface IRequestValidator<TRequest, TResponse> where TRequest : RequestBase<TResponse> where TResponse : ResponseBase
    {
        Task<TResponse> ValidateAsync(TRequest request, CancellationToken cancellationToken = default);
    }
}
