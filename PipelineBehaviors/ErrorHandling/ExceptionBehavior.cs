﻿namespace HS.Mediator.Common
{
    using System;
    using System.Diagnostics;
    using System.Threading;
    using System.Threading.Tasks;

    using MediatR;

    using Serilog;

    public class ExceptionBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
        where TRequest : RequestBase<TResponse>
        where TResponse : ResponseBase, new()
    {
        private readonly ILogger _logger;

        public ExceptionBehavior(ILogger logger)
        {
            _logger = logger;
        }

        public virtual async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            try
            {
                return await next();
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToStringDemystified());

                return new TResponse { ErrorMessage = ex.Message };
            }
        }
    }
}