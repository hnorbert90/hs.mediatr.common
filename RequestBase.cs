﻿namespace HS.Mediator.Common
{
    using MediatR;

    public abstract class RequestBase<TResponse> : IRequest<TResponse> where TResponse : ResponseBase
    {
        public long? LoggedUserID { get; set; }
    }
}