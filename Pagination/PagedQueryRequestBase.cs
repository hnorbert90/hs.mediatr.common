﻿namespace HS.Mediator.Common
{
    using HS.Common.Abstractions.Pagination;

    public abstract class PagedQueryRequestBase<TResponse> : RequestBase<TResponse>, IPagedQueryRequest where TResponse : ResponseBase
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
