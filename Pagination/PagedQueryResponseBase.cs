﻿namespace HS.Mediator.Common
{
    using HS.Common.Abstractions.Pagination;

    public abstract class PagedQueryResponseBase<TRecord> : ListQueryResponseBase<TRecord>, IPagedQueryResponse where TRecord : class, new()
    {
        public int TotalPages { get; set; }
    }
}
