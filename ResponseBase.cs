﻿namespace HS.Mediator.Common
{
    using System.Threading.Tasks;

    public class ResponseBase
    {
        public virtual string ErrorMessage { get; set; }
        public virtual bool HasError => !string.IsNullOrWhiteSpace(ErrorMessage);

        public static ResponseBase Ok() => new ResponseBase();
        public static ResponseBase Error(string errorMessage) => new ResponseBase { ErrorMessage = errorMessage };

        public static implicit operator Task<ResponseBase>(ResponseBase response)
        {
            return Task.FromResult(response);
        }
    }

    public class ResponseBase<TResult> : ResponseBase
    {
        public virtual TResult Result { get; set; }
        public static ResponseBase<TResult> Ok(TResult result) => new ResponseBase<TResult> { Result = result };

        public static implicit operator Task<ResponseBase<TResult>>(ResponseBase<TResult> response)
        {
            return Task.FromResult(response);
        }
    }
}